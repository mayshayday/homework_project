<?php session_start();

?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Main Page</title>

	<!--===============================================================================================-->	
		<link rel="icon" type="image/png" href="tableStyle/images/icons/favicon.ico"/>
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="tableStyle/vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="tableStyle/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="tableStyle/vendor/animate/animate.css">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="tableStyle/vendor/select2/select2.min.css">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="tableStyle/vendor/perfect-scrollbar/perfect-scrollbar.css">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="tableStyle/css/util.css">
		<link rel="stylesheet" type="text/css" href="tableStyle/css/main.css">
	<!--===============================================================================================-->

</head>
<body>

	<?php

		if(isset($_SESSION['use'])) 
		{
			echo '<a href="userPage.php style="font-size:25px;">My page</a><br>';
			echo '<a href="logout.php" style="font-size:25px;">Log out</a><br>';
			echo '<br><br><br>';
		}

		else 
		{
			echo '<a href="login.html" style="font-size:25px;">Login</a>';
			echo '<br><br><br>';
		}
	?>

	

	<!-- Displaying table -->

	<?php  

		echo '<label style="color:red;font-size:25px;">All Projects</label>';
		echo "<br><br>";

		$conn=mysqli_connect("mysql-kenanmamedov0.alwaysdata.net", "214856", "QwErTy!2#4%6&8(0", "kenanmamedov0_crowdfunding");
		mysqli_set_charset($conn,"utf8");

		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}

		$query = "SELECT projects.projectName, projects.projectDescription, projects.projectStartDate, projects.projectEndDate, projects.requestedFund, SUM(projects_investors.investmentFund) AS 'Total funds invested' FROM projects LEFT JOIN projects_investors ON projects_investors.idProject = projects.idProject GROUP BY projects.projectName;";
		$result=mysqli_query($conn,$query);
		$s=mysqli_fetch_assoc($result);
		

		echo "<table border=2>";
		echo '<tr class="limiter" class="container-table100" class="wrap-table100" class="table100 ver1" class="table100-firstcol" class="row100 head">';
	  	foreach ($s as $key => $value) {
	    	echo '<th class="cell100 column6">'.$key."</th>";
	  	}
		echo "</tr>";

		echo "<tbody>";
		while ($s=mysqli_fetch_assoc($result)) {
		  	echo '<tr class="limiter" class="container-table100" class="wrap-table100" class="table100 ver1" class="wrap-table100-nextcols js-pscroll" class="table100-nextcols" class="row100 body" >';
		    foreach ($s as $key => $value) {
		    	echo '<td class="cell100 column5">'.$value."</td>";
		    }
		  	echo "</tr>";
		}

		echo "</td>";
		echo "</table>";

		echo "<br><br>";

		mysqli_close($conn);
	?>

	<script src="tableStyle/vendor/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
		<script src="tableStyle/vendor/bootstrap/js/popper.js"></script>
		<script src="tableStyle/vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
		<script src="tableStyle/vendor/select2/select2.min.js"></script>
	<!--===============================================================================================-->
		<script src="tableStyle/vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
		<script>
			$('.js-pscroll').each(function(){
				var ps = new PerfectScrollbar(this);

				$(window).on('resize', function(){
					ps.update();
				})

				$(this).on('ps-x-reach-start', function(){
					$(this).parent().find('.table100-firstcol').removeClass('shadow-table100-firstcol');
				});

				$(this).on('ps-scroll-x', function(){
					$(this).parent().find('.table100-firstcol').addClass('shadow-table100-firstcol');
				});

			});

			
			
			
		</script>
	<!--===============================================================================================-->
		<script src="tableStyle/js/main.js"></script>



</body>
</html>


